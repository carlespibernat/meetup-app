<?php

namespace AppBundle\Utils;

use AppBundle\Model\Geolocalization;

class Geolocalizer
{
    /**
     * Get user geolocalitzation
     *
     * @return Geolocalization
     */
    public function getGeolocalization(): Geolocalization
    {
        $ip = $this->getUserIP();
        $details = json_decode(file_get_contents("http://freegeoip.net/json/$ip"));

        if (!isset($details->latitude) || $details->latitude == 0) {
            $details = json_decode(file_get_contents("http://freegeoip.net/json"));
        }

        $geolocalitzation = new Geolocalization();

        $geolocalitzation->setLatitude($details->latitude);
        $geolocalitzation->setLongitude($details->longitude);

        return $geolocalitzation;
    }

    /**
     * Get user ip
     *
     * @return string
     */
    private function getUserIP()
    {
        if (isset($_SERVER["HTTP_CLIENT_IP"]))
        {
            return $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
        {
            return $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])) {
            return $_SERVER["HTTP_FORWARDED"];
        }  else {
            return $_SERVER["REMOTE_ADDR"];
        }
    }
}