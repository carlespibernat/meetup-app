<?php

namespace AppBundle\Utils;

use AppBundle\Model\Event;
use AppBundle\Model\Group;
use AppBundle\Model\Venue;
use GuzzleHttp\Client;

class MeetupApi
{
    const API_ENDPOINT = "https://api.meetup.com";

    private $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Find events
     *
     * @param array $parameters
     * @see https://secure.meetup.com/meetup_api/docs/find/events/ To use parameters
     *
     * @return array
     */
    public function find(array $parameters = array()): array
    {
        $response = $this->getRequest("find/events", $parameters);

        $returnObjects = json_decode($response->getBody());

        $events = array();
        foreach ($returnObjects as $returnObject) {
            $events[] = $this->parseEvent($returnObject);
        }

        return $events;
    }

    public function getEvent($urlName, $id, $parameters = array())
    {
        $response = $this->getRequest("$urlName/events/$id");

        return $this->parseEvent(json_decode($response->getBody()));
    }

    /**
     * Parse json event to object event
     *
     * @param \stdClass $object
     *
     * @return Event
     */
    private function parseEvent(\stdClass $object)
    {
        $event = new Event();

        if (isset($object->created)) {
            $event->setCreatedAt(\DateTime::createFromFormat('U', trim((string) $object->created, 0)));
        }

        if (isset($object->name)) {
            $event->setName($object->name);
        }

        if (isset($object->duration)) {
            $d1 = new \DateTime();
            $d2 = new \DateTime();
            $d2->add(\DateInterval::createFromDateString($object->duration / 1000 . " seconds"));
            $interval = $d2->diff($d1);
            $event->setDuration($interval);
        }

        if (isset($object->id)) {
            $event->setId($object->id);
        }

        if (isset($object->status)) {
            $event->setStatus($object->status);
        }

        if (isset($object->time)) {
            $event->setStartingTime(\DateTime::createFromFormat(
                'U',
                    substr((string) $object->time, 0, -3)));
        }

        if (isset($object->updated)) {
            $event->setUpdatedAt(\DateTime::createFromFormat('U', trim((string) $object->updated, 0)));
        }

        if (isset($object->description)) {
            $event->setDescription($object->description);
        }

        if (isset($object->venue)) {
            $event->setVenue($this->parseVenue($object->venue));
        }

        if (isset($object->group)) {
            $event->setGroup($this->parseGroup($object->group));
        }

        return $event;
    }

    /**
     * Parse json group to object group
     *
     * @param \stdClass $object
     *
     * @return Group
     */
    private function parseGroup(\stdClass $object)
    {
        $group = new Group();

        if (isset($object->name)) {
            $group->setName($object->name);
        }

        if (isset($object->id)) {
            $group->setId($object->id);
        }

        if (isset($object->urlname)) {
            $group->setUrlName($object->urlname);
        }

        return $group;
    }

    /**
     * Parse json venue to object venue
     *
     * @param \stdClass $object
     *
     * @return Venue
     */
    private function parseVenue(\stdClass $object)
    {
        $venue = new Venue();

        if (isset($object->id)) {
            $venue->setId($object->id);
        }

        if (isset($object->name)) {
            $venue->setName($object->name);
        }

        if (isset($object->lat)) {
            $venue->setLatitude($object->lat);
        }

        if (isset($object->lon)) {
            $venue->setLongitude($object->lon);
        }

        if (isset($object->address_1)) {
            $venue->setAddress($object->address_1);
        }

        if (isset($object->city)) {
            $venue->setCity($object->city);
        }

        if (isset($object->country)) {
            $venue->setCountry($object->country);
        }

        return $venue;
    }

    /**
     * Performs a get request to meetup api
     *
     * @param string $path
     * @param array $parameters
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function getRequest(string $path, array $parameters = array())
    {
        $client = new Client();

        $parameters["key"] = $this->apiKey;

        return $client->get(self::API_ENDPOINT . DIRECTORY_SEPARATOR . $path, array(
            'query' => $parameters
        ));
    }
}