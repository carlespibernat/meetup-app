<?php

namespace AppBundle\Controller;

use AppBundle\Utils\Geolocalizer;
use AppBundle\Utils\MeetupApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $apiClient = new MeetupApi($this->getParameter('meetup_api_key'));

        $geolocalizer = new Geolocalizer();
        $geolocalitzation = $geolocalizer->getGeolocalization();
        $events = $apiClient->find(array(
            "lat" => $geolocalitzation->getLatitude(),
            "lon" => $geolocalitzation->getLongitude()
        ));

        return $this->render('@App/index.html.twig', [
            "events" => $events
        ]);
    }

    /**
     * @Route("event/{urlName}/{id}", name="event")
     */
    public function eventAction($urlName, $id)
    {
        $apiClient = new MeetupApi($this->getParameter('meetup_api_key'));

        $event = $apiClient->getEvent($urlName, $id);

        return $this->render('@App/event.html.twig', ['event' => $event]);
    }
}
