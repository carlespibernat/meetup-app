<?php

namespace AppBundle\Model;

class Event
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var \DateTime */
    private $createdAt;

    /** @var \DateInterval  */
    private $duration;

    /** @var string */
    private $status;

    /** @var  \DateTime */
    private $startingTime;

    /** @var  \DateTime */
    private $updatedAt;

    /** @var  string */
    private $description = '';

    /** @var  Venue */
    private $venue;

    /** @var  Group */
    private $group;

    public function __construct()
    {
        $this->setVenue(new Venue());
        $this->setGroup(new Group());
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateInterval
     */
    public function getDuration(): \DateInterval
    {
        return $this->duration;
    }

    /**
     * @param \DateInterval $duration
     */
    public function setDuration(\DateInterval $duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getStartingTime(): \DateTime
    {
        return $this->startingTime;
    }

    /**
     * @param \DateTime $startingTime
     */
    public function setStartingTime(\DateTime $startingTime)
    {
        $this->startingTime = $startingTime;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return Venue
     */
    public function getVenue(): Venue
    {
        return $this->venue;
    }

    /**
     * @param Venue $venue
     */
    public function setVenue(Venue $venue)
    {
        $this->venue = $venue;
    }

    /**
     * @return Group
     */
    public function getGroup(): Group
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup(Group $group)
    {
        $this->group = $group;
    }
}