<?php

namespace AppBundle\Model;

class Group
{
    /** @var  \DateTime */
    private $cratedAt;

    /** @var  string */
    private $name;

    /** @var  string */
    private $id;

    /** @var  string */
    private $urlName;

    /**
     * @return \DateTime
     */
    public function getCratedAt(): \DateTime
    {
        return $this->cratedAt;
    }

    /**
     * @param \DateTime $cratedAt
     */
    public function setCratedAt(\DateTime $cratedAt)
    {
        $this->cratedAt = $cratedAt;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrlName(): string
    {
        return $this->urlName;
    }

    /**
     * @param string $urlName
     */
    public function setUrlName(string $urlName)
    {
        $this->urlName = $urlName;
    }
}